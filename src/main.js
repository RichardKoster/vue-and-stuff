import Vue from 'vue'
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { far } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Toasted from 'vue-toasted';
import VueRouter from 'vue-router';
import PopularMovies from './components/PopularMovies';
import FavoriteMovies from './components/FavoriteMovies';
import User from './components/User';

library.add(far);

Vue.component('fa-icon', FontAwesomeIcon);

Vue.mixin({
  data: function() {
    return {
      get baseUrl() {
        return 'https://api.themoviedb.org/3/';
      },
      get apiKey() {
        return '7d8f3762642c475f4c3a5504bf5e540d';
      },
      get sessionToken() {
        return localStorage.getItem('sessionToken');
      },
      get account() {
        return JSON.parse(localStorage.getItem('account'));
      }
    }
  },
  methods: {
    apiUrl: function (method) {
      return this.baseUrl + method + '?api_key=' + this.apiKey;
    },
    mediaUrl: function(path, size) {
      return 'http://image.tmdb.org/t/p/' + size + path
    },
    apiAuthUrl: function (method) {
      return this.apiUrl(method) + '&session_id=' + this.sessionToken
    }
  }
})

Vue.use(Toasted);
Vue.use(VueRouter);

const routes = [
  { path: '/', component: PopularMovies },
  { path: '/user', component: User },
  { path: '/popular', component: PopularMovies },
  { path: '/favorites', component: FavoriteMovies }
]

const router = new VueRouter({
  routes
});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
